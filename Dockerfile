# pull official base image
FROM node:17-alpine

RUN apk add --no-cache bash curl 

# set working directory
WORKDIR /app

# add `/app/node_modules/.bin` to $PATH
ENV PATH /app/node_modules/.bin:$PATH

COPY package.json .
COPY ./public /app/public

# install app dependencies
RUN npm install react-router-dom --save
RUN npm install react --save
RUN npm install react-app-rewired --save
RUN npm install history --save
RUN npm install react-scripts --save
RUN npm install node-sass --save-dev
RUN npm install axios --save
RUN npm install react-icons --save
RUN npm install react-dom --save
RUN npm install interweave --save
RUN npm install --save-dev @testing-library/jest-dom
RUN npm install --save-dev @testing-library/react
RUN npm install --save-dev @testing-library/user-event


RUN npm i -g npm-check-updates
RUN ncu -u
RUN npm install

RUN node -v

RUN npm i -g serve

CMD ["npm","start"]


